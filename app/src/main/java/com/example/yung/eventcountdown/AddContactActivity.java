//AddContacActivity
package com.example.yung.eventcountdown;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

public class AddContactActivity extends AppCompatActivity {
    private EditText etName;
    private EditText etEmail;
    private EditText etDob;
    private boolean save = false;
    private SharedPreferences sp;
    private SharedPreferences.Editor spe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        sp = getSharedPreferences("spSaveState", Context.MODE_PRIVATE);
        spe = sp.edit();
        etName = (EditText) AddContactActivity.this.findViewById(R.id.name);
        etEmail = (EditText) AddContactActivity.this.findViewById(R.id.email);
        etDob = (EditText) AddContactActivity.this.findViewById(R.id.dob);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = etName.getText().toString();
                String email = etEmail.getText().toString();
                String dob = etDob.getText().toString();
                Contact contact = new Contact(name, email, dob);
                ContactDbQueries dbq = new ContactDbQueries(new ContactDbHelper(getApplicationContext()));
                if (dbq.insert(contact) != 0) {
                    save = true;
                }
                finish();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (save) {
            spe.clear();
        } else {
            String name = etName.getText().toString();
            String email = etEmail.getText().toString();
            String dob = etDob.getText().toString();

            spe.putString("SAVE_STATE_NAME", name);
            spe.putString("SAVE_STATE_EMAIL", email);
            spe.putString("SAVE_STATE_DOB", dob);
        }
        spe.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        String name = sp.getString("SAVE_STATE_NAME", "");
        String email = sp.getString("SAVE_STATE_EMAIL", "");
        String dob = sp.getString("SAVE_STATE_DOB", "");
        etName.setText(name);
        etEmail.setText(email);
        etDob.setText(dob);
    }

    public void showDatePickerDialog(View view) {
        DialogFragment fragment = new DatePickerFragment();
        fragment.show(getSupportFragmentManager(), "datePicker");
    }
}
