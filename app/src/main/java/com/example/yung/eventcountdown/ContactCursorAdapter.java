//ContactCursorAdapter
package com.example.yung.eventcountdown;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

/**
 * Created by yung on 2017/8/20.
 */
public class ContactCursorAdapter extends CursorAdapter {
    private LayoutInflater inflater;

    public ContactCursorAdapter(Context context, Cursor cursor, int flags) {
        super(context, cursor, flags);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void bindView(View view, Context context, Cursor cursor) {
        TextView tvName = (TextView) view.findViewById(R.id.item_name);
        TextView tvEmail = (TextView) view.findViewById(R.id.item_email);
        TextView tvDob = (TextView) view.findViewById(R.id.item_dob);
        String name = cursor.getString(cursor.getColumnIndex(ContactContract.ContactEntry.COLUMN_NAME_NAME));
        String email = cursor.getString(cursor.getColumnIndex(ContactContract.ContactEntry.COLUMN_NAME_EMAIL));
        String dob = cursor.getString(cursor.getColumnIndex(ContactContract.ContactEntry.COLUMN_NAME_DOB));
        tvName.setText(name);
        tvEmail.setText(email);
        tvDob.setText(dob);
    }

    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return inflater.inflate(R.layout.contact_list_item, parent, false);
    }
}