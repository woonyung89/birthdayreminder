//EditContactActivity
package com.example.yung.eventcountdown;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

public class EditContactActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_contact);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Intent intent = getIntent();
        final Contact contact = (Contact) intent.getSerializableExtra(ViewContactActivity.EXTRA_CONTACT);
        final EditText etName = (EditText) findViewById(R.id.name);
        final EditText etEmail = (EditText) findViewById(R.id.email);
        final EditText etDob = (EditText) findViewById(R.id.dob);
        etName.setText(contact.getName());
        etEmail.setText(contact.getEmail());
        etDob.setText(contact.getDob());
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                contact.setName(etName.getText().toString());
                contact.setEmail(etEmail.getText().toString());
                contact.setDob(etDob.getText().toString());

                ContactDbQueries dbq = new ContactDbQueries(new ContactDbHelper(getApplicationContext()));
                dbq.update(contact);
                finish();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void showDatePickerDialog(View view) {
        DialogFragment fragment = new DatePickerFragment();
        fragment.show(getSupportFragmentManager(), "datePicker");
    }
}
