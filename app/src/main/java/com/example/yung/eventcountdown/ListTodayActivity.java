//ListTodayActivity
package com.example.yung.eventcountdown;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ListTodayActivity extends AppCompatActivity {
    public static final String EXTRA_ID = "com.example.yung.eventcountdown.ID";
    private ListView contactList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_contact);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AddContactActivity.class);
                startActivity(intent);
            }
        });
        contactList = (ListView) findViewById(R.id.contact_list);
        contactList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                               @Override
                                               public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                                   Cursor cursor = (Cursor) parent.getItemAtPosition(position);

                                                   Intent intent = new Intent(ListTodayActivity.this, ViewContactActivity.class);
                                                   intent.putExtra(EXTRA_ID, cursor.getLong(cursor.getColumnIndex(ContactContract.ContactEntry._ID)));
                                                   ListTodayActivity.this.startActivity(intent);
                                               }
                                           }
        );
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        ContactDbQueries dbq = new ContactDbQueries(new ContactDbHelper(getApplicationContext()));
        Calendar calendar = Calendar.getInstance();
        Calendar calendar_tmr = Calendar.getInstance();
        calendar_tmr.add(Calendar.DAY_OF_MONTH, 1);
        Calendar calendar_tmr2 = Calendar.getInstance();
        calendar_tmr2.add(Calendar.DAY_OF_MONTH, 2);
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM");
        String date = sdf.format(calendar.getTime());
        String today = "%" + date + "%";
        String selection = "dob like ?";
        String[] where = {
                today
        };
        String[] columns = {
                ContactContract.ContactEntry._ID,
                ContactContract.ContactEntry.COLUMN_NAME_NAME,
                ContactContract.ContactEntry.COLUMN_NAME_EMAIL,
                ContactContract.ContactEntry.COLUMN_NAME_DOB
        };
        Cursor cursor = dbq.query(columns, selection, where, null, null, ContactContract.ContactEntry.COLUMN_NAME_DOB + " ASC");
        ContactCursorAdapter adapter = new ContactCursorAdapter(this, cursor, 0);
        contactList.setAdapter(adapter);
    }
}

