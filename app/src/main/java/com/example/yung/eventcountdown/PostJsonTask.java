//ListTodayActivity
package com.example.yung.eventcountdown;

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by James Ooi on 9/8/2017.
 */

public class PostJsonTask extends AsyncTask<Void, Void, JSONObject> {
    private static final String JSON_URL = "http://labs.jamesooi.com/uecs3253-asg.php";
    private MainActivity activity;

    public PostJsonTask(MainActivity activity) {
        this.activity = activity;
    }

    @Override
    protected JSONObject doInBackground(Void... v) {
        JSONObject response = null;
        try {
            response = postJson();
        } catch (IOException ex) {
            Log.e("IO_EXCEPTION", ex.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    protected void onPostExecute(JSONObject response) {
        try {
            //Log.d("TIMESTAMP", Long.toString(response.getLong("timestamp")));
            //Log.d("SUCCESS", Boolean.toString(response.getBoolean("success")));
            Log.d("recordsSynced", response.getString("recordsSynced"));

        } catch (Exception ex) {
            Log.e("JSON_EXCEPTION", ex.toString());
        }
    }

    private JSONObject postJson() throws Exception {
        InputStream is = null;
        OutputStream os = null;
        ContactDbQueries dbq = new ContactDbQueries(new ContactDbHelper(activity.getApplicationContext()));
        String[] columns = {
                ContactContract.ContactEntry._ID,
                ContactContract.ContactEntry.COLUMN_NAME_NAME,
                ContactContract.ContactEntry.COLUMN_NAME_EMAIL,
                ContactContract.ContactEntry.COLUMN_NAME_DOB
        };
        String[] test = {"aaa"};
        Cursor cursor = dbq.query(columns, null, null, null, null, ContactContract.ContactEntry.COLUMN_NAME_NAME + " ASC");
        JSONArray postDataArr = new JSONArray();
        if (cursor.getCount() > 0) {
            try {
                while (cursor.moveToNext()) {
                    long id_long = cursor.getLong(cursor.getColumnIndex(ContactContract.ContactEntry._ID));
                    String id = Long.toString(id_long);
                    JSONObject postData = new JSONObject();
                    postData.put("id", id);
                    postData.put("name", cursor.getString(cursor.getColumnIndex(ContactContract.ContactEntry.COLUMN_NAME_NAME)));
                    postData.put("email", cursor.getString(cursor.getColumnIndex(ContactContract.ContactEntry.COLUMN_NAME_EMAIL)));
                    postData.put("dob", cursor.getString(cursor.getColumnIndex(ContactContract.ContactEntry.COLUMN_NAME_DOB)));
                    postDataArr.put(postData);
                }
                URL url = new URL(JSON_URL);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                // Starts the query
                os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataArr));
                writer.flush();
                writer.close();
                os.close();
                int responseCode = conn.getResponseCode();
                if (responseCode == 200) {
                    is = conn.getInputStream();
                    // Convert the InputStream into ArrayList<Person>
                    return readInputStream(is);
                } else {
                    Log.e("HTTP_ERROR", Integer.toString(responseCode));
                    return null;
                }
            } catch (Exception ex) {
                Log.e("EXCEPTION", ex.toString());
                return null;
            } finally {
                if (is != null) {
                    is.close();
                }
            }
        } else {
            Log.e("HTTP_ERROR", "wtf");
        }
        return null;
    }

    public JSONObject readInputStream(InputStream is)
            throws IOException, JSONException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        StringBuilder builder = new StringBuilder();
        String input;
        while ((input = reader.readLine()) != null)
            builder.append(input);
        return new JSONObject(builder.toString());
    }

    private String getPostDataString(JSONArray data) throws Exception {
        StringBuilder result = new StringBuilder();
        result.append(URLEncoder.encode("data", "UTF-8"));
        result.append("=");
        result.append(URLEncoder.encode(data.toString(), "UTF-8"));
        return result.toString();
    }
}
